package com.android.justika


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import org.hamcrest.Matchers
import org.hamcrest.Matchers.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class SignInInstumentedTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(SignInActivity::class.java)

    @Test
    fun checkUsernameIsDisplayed() {
        onView(withId(R.id.emailForm)).check(ViewAssertions.matches(isDisplayed()))
    }

    @Test
    fun checkPasswordIsDisplayed() {
        onView(withId(R.id.passForm)).check(ViewAssertions.matches(isDisplayed()))
    }

    @Test
    fun testImageLogo() {
        onView(withId(R.id.imageView)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.imageView)).check(ViewAssertions.matches(Matchers.notNullValue()))
    }

    @Test
    fun testEditTextEmail() {
        onView(withId(R.id.emailForm)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.emailForm)).check(ViewAssertions.matches(notNullValue()))
    }

    @Test
    fun testEditTextPassword() {
        onView(withId(R.id.passForm)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.passForm)).check(ViewAssertions.matches(notNullValue()))
    }

    @Test
    fun testEmailField() {
        onView(withId(R.id.emailForm)).check(ViewAssertions.matches(notNullValue()))
        onView(withId(R.id.emailForm)).check(ViewAssertions.matches(withHint("email")))
    }

    @Test
    fun testPasswordField() {
        onView(withId(R.id.passForm)).check(ViewAssertions.matches(notNullValue()))
        onView(withId(R.id.passForm)).check(ViewAssertions.matches(withHint("password")))
    }

    @Test
    fun testMasukButton() {
        onView(withId(R.id.loginButton)).check(ViewAssertions.matches(notNullValue()));
        onView(withId(R.id.loginButton)).check(ViewAssertions.matches(withText("MASUK")));

    }
}