package com.android.justika

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeLeft
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import org.hamcrest.Matchers.notNullValue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class TabActivityInstrumentedTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(TabActivity::class.java)

    @Test
    fun testTabActivity() {
        onView(withId(R.id.view_pager))
            .check(matches(isDisplayed()))
        onView(withId(R.id.view_pager))
            .perform(swipeLeft())
    }

    @Test
    fun checkTabDisplayed() {
        onView(withId(R.id.tabs))
            .perform(click())
            .check(matches(isDisplayed()))
    }

}