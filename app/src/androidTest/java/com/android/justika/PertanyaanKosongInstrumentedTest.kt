package com.android.justika

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import org.hamcrest.Matchers.notNullValue
import org.junit.Test
import org.junit.runner.RunWith



@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class PertanyaanKosongInstrumentedTest {
    @Test fun testPertanyaanText() {
        launchFragmentInContainer<PertanyaanFragment>()
        onView(withId(R.id.pertanyaan_kosong)).check(matches(isDisplayed()))
        onView(withId(R.id.pertanyaan_kosong)).check(matches(notNullValue()))
        onView(withId(R.id.pertanyaan_kosong)).check(matches(withText("Pertanyaan Kosong")))
    }

    @Test fun testImageKosong() {
        launchFragmentInContainer<PertanyaanFragment>()
        onView(withId(R.id.image_kosong)).check(matches(isDisplayed()))
        onView(withId(R.id.image_kosong)).check(matches(notNullValue()))
    }
}