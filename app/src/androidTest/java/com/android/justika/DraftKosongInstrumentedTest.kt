package com.android.justika

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import org.hamcrest.Matchers.notNullValue
import org.junit.Test
import org.junit.runner.RunWith



@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class DraftKosongInstrumentedTest {
    @Test fun testDraftText() {
        launchFragmentInContainer<DraftFragment>()
        onView(withId(R.id.draft_kosong)).check(matches(isDisplayed()))
        onView(withId(R.id.draft_kosong)).check(matches(notNullValue()))
        onView(withId(R.id.draft_kosong)).check(matches(withText("Draft Kosong")))
    }

    @Test fun testImageDraftKosong() {
        launchFragmentInContainer<DraftFragment>()
        onView(withId(R.id.image_draftkosong)).check(matches(isDisplayed()))
        onView(withId(R.id.image_draftkosong)).check(matches(notNullValue()))
    }
}