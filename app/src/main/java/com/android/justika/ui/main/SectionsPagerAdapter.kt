package com.android.justika.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.android.justika.DraftFragment
import com.android.justika.PertanyaanFragment
import com.android.justika.TerjawabFragment


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val pages = listOf(
        PertanyaanFragment(),
        DraftFragment(),
        TerjawabFragment()
    )
    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Pertanyaan"
            1 -> "Draft"
            2 -> "Terjawab"
            else -> "Fourth Tab"
        }
    }

    override fun getCount(): Int {
        return pages.size
    }
}