package com.android.justika

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.android.justika.ApiUtils.apiService
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import java.io.IOException
import java.lang.Exception

class SignInActivity : AppCompatActivity() {

    private var loginButton: Button? = null
    private var emailText: EditText? = null
    private var passwordText: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        loginButton = findViewById<Button>(R.id.loginButton)
        emailText = findViewById<EditText>(R.id.emailForm)
        passwordText = findViewById<EditText>(R.id.passForm)



        loginButton!!.setOnClickListener(){
            try {
                login()
            }
            catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun login() {
        val json = JSONObject()

        try {
            val username = emailText?.text.toString()
            val password = passwordText?.text.toString()
            Log.d("Login attempt", "attempting")
            json.put("username", username)
            json.put("password", password)
            val requestBody: RequestBody =
                json.toString().toRequestBody("application/json".toMediaTypeOrNull())
            val call: Call<LoginResponse> = apiService.postUserCredentials(requestBody)
            call.enqueue(
                object : Callback<LoginResponse> {
                    override fun onResponse(
                        call: Call<LoginResponse>?,
                        response: retrofit2.Response<LoginResponse>?
                    ) {
                        if (response!!.isSuccessful) {
                            Toast.makeText(this@SignInActivity, response.code().toString(), Toast.LENGTH_LONG).show()
                            val intent = Intent(this@SignInActivity, TabActivity::class.java)
                            intent.putExtra("jsonObject", response.toString())
                            startActivity(intent)
                            finish()
                        }
                    }

                    override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                        Log.d("loginAuthendication", "failure--" + t.toString())
                        Toast.makeText(this@SignInActivity, "Failed to login", Toast.LENGTH_LONG).show()
                    }
                }
            )
        }

        catch (e: Exception) {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
        }
    }


}


