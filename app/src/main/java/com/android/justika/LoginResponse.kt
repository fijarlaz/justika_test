package com.android.justika

import com.google.gson.annotations.SerializedName

class LoginResponse {
    @SerializedName("token")
    var token: String? = null
    @SerializedName("user_id")
    var user_id: String? = null
    @SerializedName("email")
    var email: String? = null
    @SerializedName("lawyer_name")
    var lawyer_name: String? = null
    @SerializedName("lawyer_code")
    var lawyer_code: String? = null
}