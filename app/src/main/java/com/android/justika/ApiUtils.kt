package com.android.justika

object ApiUtils {
    var BASE_URL = "https://staging-api.justika.com"

    val apiService: ApiService
    get() = RetrofitClient.getApi(BASE_URL)!!.create(ApiService::class.java)
}