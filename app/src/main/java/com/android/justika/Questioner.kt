package com.android.justika

import com.google.gson.annotations.SerializedName

class Questioner {
    @SerializedName("code")
    var code: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("phone_number")
    var phoneNumber: String? = null
}