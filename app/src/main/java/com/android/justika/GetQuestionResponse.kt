package com.android.justika

import com.google.gson.annotations.SerializedName

class GetQuestionResponse {
    @SerializedName("next")
    var next: Int? = null

    @SerializedName("previous")
    var previous: Int? = null

    @SerializedName("count")
    var count: Int? = null

    @SerializedName("result")
    var result = arrayOf<Result>()

}