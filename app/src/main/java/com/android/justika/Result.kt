package com.android.justika

import com.google.gson.annotations.SerializedName

class Result {
    @SerializedName("question_code")
    var questionCode: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("questioner")
    var questioner: Questioner? = null

    @SerializedName("article")
    var article: String? = null

    @SerializedName("parent_question")
    var parentQuestion: Int? = null

    @SerializedName("question_status")
    var questionStatus: QuestionStatus? = null

    @SerializedName("related_article")
    var relatedArticle: Int? = null

    @SerializedName("is_private")
    var isPrivate: Boolean? = null

    @SerializedName("notes")
    var notes: String? = null

    @SerializedName("answers")
    var answers: String? = null

    @SerializedName("created_at")
    var createdAt: String? = null



}