package com.android.justika

import com.google.gson.annotations.SerializedName

class Answer {
    @SerializedName("description")
    var description: String? = null

    @SerializedName("description_html")
    var descriptionHtml: String? = null

    @SerializedName("question")
    var question: Int? = null
}