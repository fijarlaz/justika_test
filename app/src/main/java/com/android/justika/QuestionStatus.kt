package com.android.justika

import com.google.gson.annotations.SerializedName

class QuestionStatus {
    @SerializedName("name")
    var name: String? = null
}