package com.android.justika

import com.google.gson.annotations.SerializedName

class FailLoginResponse {
    @SerializedName("non_field_errors")
    var non_field_errors: Array<String>? = null
}