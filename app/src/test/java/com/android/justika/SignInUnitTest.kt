package com.android.justika

import android.content.Context
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import io.mockk.mockkClass
import assertk.assertions.isNotEqualTo
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.mockwebserver.MockWebServer
import org.json.JSONArray
import org.json.JSONObject
import org.junit.After
import org.junit.Before
import org.mockito.Mockito.`when`
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


const val FAIL_LOGIN_RESPONSE = "Unable to log in with provided credentials."

@RunWith(MockitoJUnitRunner::class)
class SignInUnitTest {

    lateinit var mockWebServer: MockWebServer
    var loginResponse: Response<LoginResponse>? = null

    @Mock
    private lateinit var mockLogin: Context

    @Before
    fun Setup() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `test login form is not null`() {
        assertThat(R.id.emailForm.toString()).isNotNull()
        assertThat(R.id.passForm.toString()).isNotNull()
    }

    @Test
    fun `test Login response is fail`() {
        val failLoginResponse = FailLoginResponse()
        failLoginResponse.non_field_errors = arrayOf("Unable to log in with provided credentials.")

        assertThat(failLoginResponse.non_field_errors!![0], FAIL_LOGIN_RESPONSE)
    }

    @Test
    fun testLogin() {
        val json = JSONObject()
        json.put("username", "justika")
        json.put("password", "justikapass")

        val requestBody: RequestBody =
            json.toString().toRequestBody("application/json".toMediaTypeOrNull())
        val call: Call<LoginResponse> = ApiUtils.apiService.postUserCredentials(requestBody)
        call.enqueue(
            object : Callback<LoginResponse> {
                override fun onResponse(
                    call: Call<LoginResponse>?,
                    response: Response<LoginResponse>?
                ) {
                    loginResponse = response
                    assertThat(response!!.code()).isNotEqualTo(200)
                }
                override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {

                }
            })
    }
}