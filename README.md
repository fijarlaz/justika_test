<div align="center"><h1>Justika Quora untuk Bantuan Hukum Mobile - Lawyer Side Android</h1></div>

[![pipeline status branch staging](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/pleasepleaselulus-justika-quora-untuk-bantuan-hukum-mobile-lawyer-side/badges/staging/pipeline.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/pleasepleaselulus-justika-quora-untuk-bantuan-hukum-mobile-lawyer-side/commits/staging)
[![coverage report branch staging](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/pleasepleaselulus-justika-quora-untuk-bantuan-hukum-mobile-lawyer-side/badges/staging/coverage.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/pleasepleaselulus-justika-quora-untuk-bantuan-hukum-mobile-lawyer-side/commits/staging)

Justika Quora - Lawyer Side merupakan proyek dari mata kuliah Pengembangan Perangkat Lunak semester genap 2019/2020. Dibuat untuk perangkat mobile Android (dengan Android Native - Java Kotlin) dan untuk perangkat mobile iOS (Swift) oleh kelompok PleasePleaseLulus dengan anggota:

- Darin Amanda Zakiya - 1706979190
- Janitra Ariena Sekarputri - 1706979316
- Khalis Murfid - 1706040164
- M. Fijar Lazuardy R. E. - 1706039471
- Yafonia Kristiani M. N. H. - 1706039521

Product Owner: Selina Maurizka
<br> Scrum Master: Nabila Fakhirah

# Table of Contents

- [Introduction](#introduction)
- [Install](#install)
- [Running Development Mode](#menjalankan-aplikasi)
- [License](#license)

# Introduction

Justika Quora - Lawyer Side merupakan aplikasi mobile yang dibuat untuk memudahkan para pengacara dan lulusan hukum terbaik menjawab pertanyaan-pertanyaan yang dikirim oleh masyarakat.


# Install

Project ini menggunakan Java Kotlin sebagai bahasa pemrograman dan Native Android untuk building environment. Terdapat beberapa requirement yang dibutuhkan untuk menjalankan dan berkontribusi dalam project ini.

- [IDE of your choice]()
  > Note: Kami memberikan rekomendasi untuk menggunakan Android Studio atau Intellij IDEA yang bisa didapatkan melalui:
  > - [Android Studio](https://developer.android.com/studio)
  > - [Intellij IDEA](https://www.jetbrains.com/idea/)
- [Java 8](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html)
  > Note: Kami memberikan rekomendasi menggunakan Java 8. Ikuti tutorial untuk install Java JDK pada komputer Anda. 
    

# Menjalankan Aplikasi

Git clone
```sh
git clone https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/pleasepleaselulus-justika-quora-untuk-bantuan-hukum-mobile/pleasepleaselulus-justika-quora-untuk-bantuan-hukum-mobile-lawyer-side.git
```

Setelah semua requirement sudah dimiliki dan berhasil melakukan clone, Anda dapat menjalankan aplikasi ini dengan
- Buka Android Studio
- Import project dari folder yang telah di clone
- Terdapat 2 pilihan untuk menjalankan aplikasi
    - Jika ingin menjalankan aplikasi langsung pada Android Studio bisa menggunakan Android Virtual Device yang dapat didapatkan melalui step pada link berikut
        - [Android Virtual Device](https://developer.android.com/studio/run/managing-avds)
    - Jika ingin menjalankan aplikasi menggunakan perangkat Android dengan menghubungkan perangkat tersebut ke komputer Anda dengan mengikuti step berikut
        - [Developer Mode](https://developer.android.com/studio/debug/dev-options)


# License

Copyright (c) 2020 PPLB7 Team & Faculty of Computer Science Universitas Indonesia.

